import {inject, Provider} from '@loopback/core';
import {getService} from '@loopback/service-proxy';
import {RestApiDataSource} from '../datasources';

export interface PlaceholderApi {
  getPosts(): Promise<any[]>;
}

export class PlaceholderApiProvider implements Provider<PlaceholderApi> {
  constructor(
    // restApi must match the name property in the datasource json file
    @inject('datasources.restApi')
    protected dataSource: RestApiDataSource = new RestApiDataSource(),
  ) {}

  value(): Promise<PlaceholderApi> {
    return getService(this.dataSource);
  }
}
