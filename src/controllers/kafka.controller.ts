import {inject} from '@loopback/core';
import {
  get,
  param,
  post,
  requestBody,
  Response,
  RestBindings
} from '@loopback/rest';
import {
  Consumer,
  KafkaClient,

  Producer, ProduceRequest
} from 'kafka-node';

const KAFKA_HOST = 'localhost:9092';

export class KafkaController {
  private client: KafkaClient;
  private producer: Producer;
  constructor(
    @inject('kafka.host', {optional: true})
    private kafkaHost: string = KAFKA_HOST,
  ) {
    // Creating connection with kafka server
    this.client = new KafkaClient({kafkaHost: this.kafkaHost});
    this.producer = new Producer(this.client);
  }
  /**
  * Wait for the producer to be ready
  */
  private isProducerReady() {
    return new Promise<void>((resolve, reject) => {
      this.producer.on('ready', () => {
        console.log("ready");

        resolve()
      });
      this.producer.on('error', err => {
        console.log('err:', err)
        reject(err)
      });
    });
  }

  private createConsumer(
    topic: string
  ) {
    const consumer = new Consumer(this.client, [{topic: topic, partition: 0}], {
      autoCommit: false
    });
    return consumer;
  }

  private writeMessageToResponse(
    consumer: Consumer,
    limit: number,
    response: Response,
  ) {
    let count = 0;
    consumer.on('message', message => {
      count++;
      response.write(`id: ${message.offset}\n`);
      response.write('event: message\n');
      response.write(`data: ${JSON.stringify(message)}\n`);
      if (count >= limit) {

        consumer.close(true, err => {
          if (err)
            console.log(
              'Something is wrong when closing the consumer.',
              err.message,
            );

          response.end();
        });
      }

    });
    return response;
  }

  /**
   * Create topics
   * @param topics
   */
  @post('/topics')
  async createTopics(
    @requestBody({
      content: {
        'application/json': {
          schema: {
            type: 'array',
            items: {type: 'string'},
          },
        },
      },
    })
    topics: string[],
  ) {
    await this.isProducerReady();
    return new Promise<any>((resolve, reject) => {
      this.producer.createTopics(topics, true, (err, data) => {
        if (err) reject(err);
        else resolve(data);
      });
    });
  }

  /**
   * Publish a message to the given topic
   * @param topic The topic name
   * @param message The message
   */
  @post('/topics/{topic}/messages')
  async publish(
    @param.path.string('topic') topic: string,
    @requestBody({
      content: {
        'application/json': {
          schema: {
            type: 'array',
            items: {type: 'string'},
          },
        },
      },
    })
    messages: string[],
  ) {
    await this.isProducerReady();
    const req: ProduceRequest = {topic, messages};
    return new Promise<any>((resolve, reject) => {
      this.producer.send([req], (err, data) => {
        if (err) reject(err);
        else resolve(data);
      });
    });
  }

  /**
   * Consume messages of a given topic
   * @param topic The topic name
   */
  @get('/topics/{topic}/messages', {
    responses: {
      '200': {
        'text/event-stream': {
          schema: {
            type: 'string',
          },
        },
      },
    },
  })
  async consumeMessagesOnTopics(
    @param.path.string('topic') topic: string,
    @param.query.string('limit') limit: number,
    @inject(RestBindings.Http.RESPONSE) response: Response,
  ) {
    let consumer = this.createConsumer(topic);

    limit = +limit || 5;
    response.setHeader('Cache-Control', 'no-cache');
    response.contentType('text/event-stream');
    return this.writeMessageToResponse(consumer, limit, response);
  }
}
