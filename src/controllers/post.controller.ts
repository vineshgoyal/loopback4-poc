import {inject} from '@loopback/core';
import {
  get,
  getModelSchemaRef
} from '@loopback/rest';
import {createClient} from 'redis';
import {promisify} from 'util';
import {Post} from '../models/post.model';
import {PlaceholderApi} from '../services';


// Creating connect with redis server
const client = createClient({
  host: "localhost",
  port: 6379,
  password: 'secret'
});

const getCache = promisify(client.get).bind(client);

export class PostController {
  constructor(
    @inject('services.PlaceholderApi')
    protected postService: PlaceholderApi,
  ) {}
  /**
   * getting list of posts
   */
  @get('/posts', {
    responses: {
      '200': {
        description: 'Posts model instance',
        content: {'application/json': {schema: getModelSchemaRef(Post)}},
      },
    },
  })
  async getPosts(): Promise<Post[]> {
    const getPostCache = await getCache("posts");
    // check if data is cached than displayed cache data to save the network bandwidth.
    if (getPostCache) {
      return JSON.parse(getPostCache);
    }
    // Fetching new data
    const postData: Post[] = await this.postService.getPosts()
    // Cache result for 20 sec.
    client.set('posts', JSON.stringify(postData), "EX", 20);
    return postData;
  }

}
