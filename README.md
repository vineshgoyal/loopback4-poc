# A POC

To build this POC i have used following tools:

- 💥 Loopback 4
- 💥 Kafka
- 💥 Redis
- 💥 MongoDB
- 💥 Docker

## System requirements

- Node >= 8.9
- Docker
- LoopBack 4 CLI

## To install dependencies

```sh
npm install
```

## To create docker containers

before execute below command make sure port 6379,9092,2181 and 27017 should be available.
This step is optional and can be skipped if you already have following tools on your system.

- Kafka
- Zookeeper
- Redis
- Mongodb

if not hit the command

```sh
npm run docker:up
```

## Start Application server

```sh
npm start
```

## Rebuild the project

To incrementally build the project:

```
npm run build
```

## A small snapshot of performance using Redis

![alt Redis-cache](./images/Redis-log.png)

> I used redis and kafka manually due to lack of documentation. I didn't find good guidelines to integrate kafka and redis with loopback 4. POC has created with minimum configuration.
